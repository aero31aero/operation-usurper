#!/bin/bash

compress-file() {
    # discovered by exhaustive conversion that the following parameters work best(ish) for us.
    magical_parameters="-define png:compression-filter=2 -define png:compression-level=9 -define png:compression-strategy=1"
    # specify the default colorspace.
    colorspace="-colorspace sRGB"
    # resize to 1066*1600; this size seems good enough for most screens.
    dimensions="-resize 1066"
    # strip the metadata before outputting for additional savings.
    strip="-strip"

    input=$1
    output=$2

    echo "compressing $input"
    convert "$input" $magical_parameters $colorspace $strip $dimensions "$output"
}

make-dirs () {
    echo "making directories"
    for i in {124..140}; do
        mkdir -p static/chapters/$i-compressed
        mkdir -p assets/masters-original/$i
    done
}

extract () {
    echo "unzipping new masters"
    rm -rf assets/masters
    unzip -q assets/masters.zip -d assets
}

get-file-list () {
    echo "making file list"
    find assets/masters-original -type f -exec md5sum {} \; | sort -k 2 | sed 's/-original//g' > assets/original.txt
    find assets/masters -type f -exec md5sum {} \; | sort -k 2 | sed 's/-original//g' > assets/new.txt
    # echo $original
    diff assets/original.txt assets/new.txt | grep '>' | cut -f 4 -d ' ' > assets/files-to-convert.txt
}

convert-files () {
    length=$(cat assets/files-to-convert.txt | wc -l)
    echo "updating $length files"
    for file in $(cat assets/files-to-convert.txt); do
        chapter=$(echo $file | sed 's/assets\/masters\///g' | cut -c 1-3)
        page=$(echo $file | sed 's/assets\/masters\///g' | cut -c 5-)
        input=$(echo assets/masters/$chapter/$page)
        output=$(echo static/chapters/$chapter-compressed/$page)
        compress-file $input $output
    done
}

clean () {
    echo "cleaning up"
    rm -r assets/masters-original
    mv assets/masters assets/masters-original
    rm assets/*.txt
}

make-dirs
extract
get-file-list
convert-files
clean
