#!/bin/bash

compress-file() {
    # discovered by exhaustive conversion that the following parameters work best(ish) for us.
    magical_parameters="-define png:compression-filter=2 -define png:compression-level=9 -define png:compression-strategy=1"
    # specify the default colorspace.
    colorspace="-colorspace sRGB"
    # resize to 1066*1600; this size seems good enough for most screens.
    dimensions="-resize 1066"
    # strip the metadata before outputting for additional savings.
    strip="-strip"

    input=$1
    output=$2

    echo "compressing $input"
    convert "$input" $magical_parameters $colorspace $strip $dimensions "$output"
}

make-dirs () {
    echo "making directories"
    mkdir -p static-ouroboros/chapters/126-compressed
        mkdir -p assets-ouroboros/masters-original/126
    for i in {134..142}; do
        mkdir -p static-ouroboros/chapters/$i-compressed
        mkdir -p assets-ouroboros/masters-original/$i
    done
}

extract () {
    echo "unzipping new masters"
    rm -rf assets-ouroboros/masters
    unzip -q assets-ouroboros/masters.zip -d assets-ouroboros
}

get-file-list () {
    echo "making file list"
    find assets-ouroboros/masters-original -type f -exec md5sum {} \; | sort -k 2 | sed 's/-original//g' > assets-ouroboros/original.txt
    find assets-ouroboros/masters -type f -exec md5sum {} \; | sort -k 2 | sed 's/-original//g' > assets-ouroboros/new.txt
    # echo $original
    diff assets-ouroboros/original.txt assets-ouroboros/new.txt | grep '>' | cut -f 4 -d ' ' > assets-ouroboros/files-to-convert.txt
}

convert-files () {
    length=$(cat assets-ouroboros/files-to-convert.txt | wc -l)
    echo "updating $length files"
    for file in $(cat assets-ouroboros/files-to-convert.txt); do
        chapter=$(echo $file | sed 's/assets-ouroboros\/masters\///g' | cut -c 1-3)
        page=$(echo $file | sed 's/assets-ouroboros\/masters\///g' | cut -c 5-)
        input=$(echo assets-ouroboros/masters/$chapter/$page)
        output=$(echo static-ouroboros/chapters/$chapter-compressed/$page)
        compress-file $input $output
    done
}

clean () {
    echo "cleaning up"
    rm -r assets-ouroboros/masters-original
    mv assets-ouroboros/masters assets-ouroboros/masters-original
    rm assets-ouroboros/*.txt
}

make-dirs
extract
get-file-list
convert-files
clean
