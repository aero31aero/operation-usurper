'use strict';

module.exports = {
    plugins: {
        // Warning: despite appearances, order is significant
        'postcss-nested': {},
        'postcss-extend-rule': {},
        'postcss-custom-media': {
            importFrom: ['src/common/index.css'],
        },
        'postcss-calc': {},
        'postcss-media-minmax': {},
        autoprefixer: {},
    },
};
