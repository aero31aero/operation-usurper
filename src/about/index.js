import './index.css';
import textFit from 'textfit';
import $ from 'jquery';
window.jQuery = $;
window.$ = $;

const rework_page_elements = () => {
    var div = $('.title-container');
    var width = div.width();
    div.css('height', width * 0.5);
    textFit(document.getElementById('desc'), {
        alignHoriz: true,
    });
};

$(window).on('resize', rework_page_elements);
$(rework_page_elements); // on ready
