import './index.css';

const get_file_list = (chapter, limit, extension) => {
    const files = [];
    for (let i = 1; i <= limit; i++) {
        const page = `${i}`.padStart(3, '0');
        files.push(
            `/static/chapters/${chapter}-compressed/${page}.${extension}`
        );
    }
    return files;
};

const preloader = async (files, root) => {
    // fetches all images sequentially, no parallel downloads
    if (files.length === 0) {
        return;
    }
    const file = files.shift();
    const img = new Image();
    img.onload = () => {
        root.appendChild(img);
        preloader(files, root);
    };
    img.id = file;
    img.classList.add('page-image');
    const page = file.split('/')[4].split('.')[0];
    img.title = `Page ${page}`;
    img.src = file;
};

const main = async () => {
    const number = document.getElementById('number').innerText;
    const limit = parseInt(document.getElementById('limit').innerText);
    const extension = document.getElementById('extension').innerText;
    const images = get_file_list(number, limit, extension);
    preloader(images, document.getElementById('pages'));
};

main();
