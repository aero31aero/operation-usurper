import './index.css';
import $ from 'jquery';

$(() => {
    // open all external links in new tab
    const all_links = document.querySelectorAll('a');
    for (let i = 0; i < all_links.length; i++) {
        const a = all_links[i];
        if (a.hostname != location.hostname) {
            a.rel = 'noopener';
            a.target = '_blank';
        }
    }
});
