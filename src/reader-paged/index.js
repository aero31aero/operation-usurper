import './index.css';
import React from 'react';
import ComicViewer from 'react-comic-viewer';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import hotkeys from 'hotkeys-js';

const setup_shortcuts = () => {
    // errors here are missing dom elements; just ignore them.
    hotkeys('left,down', () => {
        try {
            $('.fullscreen > div > div > a:nth-child(2)')[0].click();
        } catch (e) {}
    });
    hotkeys('right,up', () => {
        try {
            $('.fullscreen > div > div > a:nth-child(3)')[0].click();
        } catch (e) {}
    });
    hotkeys('f', () => {
        try {
            const fullscreen_on = $(
                'aside > div > div > button:nth-child(2)'
            )[0];
            const fullscreen_off = $(
                '.fullscreen-enabled > div > button:nth-child(2)'
            )[0];
            if (fullscreen_on) {
                fullscreen_on.click();
            } else if (fullscreen_off) {
                fullscreen_off.click();
            }
        } catch (e) {}
    });
};

const get_file_list = (chapter, limit, extension) => {
    const files = [];
    for (let i = 1; i <= limit; i++) {
        const page = `${i}`.padStart(3, '0');
        files.push(
            `/static/chapters/${chapter}-compressed/${page}.${extension}`
        );
    }
    return files;
};

const main = async () => {
    const number = document.getElementById('number').innerText;
    const limit = parseInt(document.getElementById('limit').innerText);
    const extension = document.getElementById('extension').innerText;
    const images = get_file_list(number, limit, extension);
    function App() {
        return <ComicViewer pages={images} />;
    }
    ReactDOM.render(App(), document.getElementById('app'));
    setup_shortcuts();
};

main();
