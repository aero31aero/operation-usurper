const team = {};

const add = (id, name, role, description, image, socials = []) => {
    const person = { id, name, role, description };
    person.image = `/static/team/${image}`;
    person.socials = socials.map((e) => {
        return {
            type: e[0],
            username: e[1],
        };
    });
    team[id] = person;
};

add(
    'leaks',
    'Leaks Repaired',
    'Hackerman. I maintain the tech side of things',
    'Joined fueled by hate. Stayed for the love.',
    'leaks.png',
    [['discord', 'leaks-repaired#4164']]
);

add(
    'reius',
    'Reius',
    'Storyboarder/Artist. I enjoy making layouts',
    'Learning artist and taking up projects helps me to draw more.',
    'reius.jpg',
    [['twitter', '@Reius_Rhodes']]
);

add(
    'ofek',
    'Ofek Mantzur',
    'Wallpaper Credits',
    'Oh feel free to use my painting, just credit me :) https://redd.it/oq3c88',
    'ofek.jpg',
    [
        ['twitter', '@ofekmantzur'],
        ['instagram', 'ofekmantzur'],
    ]
);

add(
    'skyclad',
    'Skyclad_Observer',
    'Director',
    'I just keep moving forward until my ideal ending is restored',
    'skyclad.jpg',
    [
        ['twitter', '@Skyclad222'],
        ['discord', 'Skyclad#3793'],
    ]
);

add(
    'dash',
    'Dashaque',
    'Artist/screentoning',
    'Wanted to get back in touch with my artistic side, and since I love AoT, thought this would be a great way to do that',
    'dash.png'
);

add(
    'chaide',
    'Chaide Orphound',
    'Artist',
    'I want to see that "scenery"',
    'Chaide.jpg',
    [
        ['twitter', '@Ophicial_Chaide'],
        ['discord', 'Chaide Orphound#4130'],
    ]
);

add(
    'hedache',
    'Hedache',
    'Artist',
    'Ambitious artist side-tracked by the shittiest ending, i keep moving forward until all canon shit is destroyed',
    'hedache.png'
);

add(
    'infinite',
    'Infinite Justice',
    'Proofreader/Community Server Mod',
    'Wanted to be part of the project to get final closure to let this story go. The ending wasn’t worth the time and hype. I believe Usurper will provide the necessary catharsis and validation the series deserved. Any other opinion is wrong and you should feel ashamed of yourself.',
    'infinite.jpg',
    [
        ['twitter', '@infin_justice'],
        ['discord', 'InfiniteJustice#1745'],
    ]
);

add(
    'noofz',
    'NoofZ',
    'Screentoner/Writer/Community Server Mod',
    "I simply wanted to rewrite AoT's crappy ending with some friends.",
    'noofz.jpg',
    [
        ['reddit', 'noofz'],
        ['discord', 'noofz#9206'],
    ]
);

add(
    'demi',
    'Mary',
    'Artist / help with typesetting sometimes',
    'I wanted to get out of my comfort zone art wise',
    'demi.png',
    [['twitter', '@birthdecay']]
);

add('zubia', 'Zubia', 'Artist', 'Candice told me to join', 'zubia.jpg', [
    ['reddit', 'zubia-'],
    ['discord', 'zuwubia#2451'],
    ['twitter', '@okzubia'],
    ['instagram', 'zerokiu__'],
]);

add(
    'hunter',
    'Hunter Jun',
    'Artist and sometimes translator',
    'I just keep trying on what I like',
    'hunterjun.jpg',
    [['twitter', '@hunterjun_']]
);

add(
    'ahri',
    'Ahri (Frances)',
    'Artist',
    'Drawing is my passion. I’ve joined to explore creativity and check out different talented artists for this project.',
    'ahri.jpg',
    [
        ['discord', 'Frances#5445'],
        ['instagram', 'ahrinators'],
    ]
);

add(
    'bbon',
    'b-bon',
    'Art Director, Artist',
    'I wanted to improve my art and contribute to a more satisfying conclusion for the manga',
    'bbon.jpg'
);

add(
    'gaige',
    'gaige',
    'Artist',
    'Saw this projects first few chapters and really wanted to be a part of it',
    'gaige.png',
    [
        ['discord', 'gaige judy#7832'],
        ['instagram', 'arty.gage'],
    ]
);

const bert_bio = `Warning: this shit's gonna be long.
At first, it came at a moment of doubt and a good tint of concern throughout the times during the Rumbling Arc's creation, be it the formation of the alliance seeming excessively rushed, the unusually cringy lines and scenes thrown in the midst of what would've been a rather desperate time, it all felt too... Marvely. And so an idea began to form, as the chapters churned further, and further, with meaningless plot holes and Rian Johnson levels of bullshit, from the wanking and the unnecessary shipping. At first it was actually a shitpost idea only shared to myself, and another who wasn't into AOT yet. 

It all was more like; How could I fix the entire arc? Would there be ways to refit the themes into what it should've been and had been upheld in previous arcs?

And then the triumvirate of horrendous chapters struck.

At first, I assumed the leaks were all but a hilarious shitpost straight from 4chan, or a doujin from a retard; oh how was I horribly wrong. 

The messiah monging of the dead, the cursed as hell necrophilia scene, the character that I had watched develop from a pansy to what would've been a complex character that had to do anything for that very freedom for the home he had grew up in, even if it meant sacrificing the world in the process reduced to a pathetic incel that would've been comparable to his earlier chapter incarnations.

The last two was the final straw, I went about on Yaegerbomb ( I know, quite expectant, and it was when OGbomb was still around ), and simply asked if some were willing to to a fanmade "ending", or perhaps..

The Complete Overhaul of the Entire Rumbling Arc. From Chapter 124.. to the very end.

It took some professional levels of monging to get it off the ground but.. here we are.

It was a rocky road leading up to now, and I trust those who had been working on this project for all times, be it from the start or just recently. As this vision wasn't just my own; it was everyone's. Those who just wanted to see a little show finally get the ending it kinda deserved in reality.

And yeah, we're not professionals, but so what about it? All that matters in the here and now, is that very fire, that passion that put the pieces together would be that very spark for everything to be in place.`;

add(
    'bertolt',
    '【◈ Bertolt Hoover ◈】',
    'Founder of the Project',
    bert_bio,
    'bertolt.png',
    [['reddit', 'Memetastic1972']]
);

module.exports = team;
