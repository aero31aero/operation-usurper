const common = require('./common');
const path = require('path');

const raw_chapter_data = [
    // chapter-number: [pages, extension, published, link(external)?]
    [124, 52, 'png', true],
    [125, 45, 'png', true],
    [126, 43, 'png', true],
    // 127 is unchanged
    [127, 'https://ww7.readsnk.com/chapter/shingeki-no-kyojin-chapter-127/'],
    [128, 52, 'png', true],
    [129, 54, 'png', true],
    [130, 39, 'png', true],
    [131, 41, 'png', true],
    [132, 57, 'png', true],
    [133, 40, 'png', true],
    [134, 0, '', false],
    [135, 0, '', false],
    [136, 0, '', false],
    [137, 0, '', false],
    [138, 0, '', false],
    [139, 0, '', false],
];

const main = async () => {
    const chapter_data = common.make_chapter_data(raw_chapter_data);
    const last_chapters = chapter_data
        .slice()
        .reverse()
        .filter((e) => e.published);

    const site = 'usurper';

    const webpack_config = Object.assign({}, common.webpack_config);

    const last_chapter = last_chapters[0];
    const second_last_chapter = last_chapters[1];

    const team = require(path.resolve(__dirname, '../src/team'));

    const tasks = [
        common.inject_entry(
            webpack_config,
            '.',
            {
                chapter_data,
                last_chapter,
                second_last_chapter,
            },
            site
        ),
        common.inject_entry(
            webpack_config,
            'about',
            {
                team: team,
            },
            site
        ),
        common.inject_entry(webpack_config, 'gallery', {}, site),
    ];
    chapter_data
        .filter((e) => e.published)
        .forEach((e) => {
            tasks.push(common.make_chapter(webpack_config, e, site));
        });
    await Promise.all(tasks);
    return webpack_config;
};

module.exports = main;
