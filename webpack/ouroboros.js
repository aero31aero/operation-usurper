const common = require('./common');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const raw_chapter_data = [
    // chapter-number: [pages, extension, published, link(external)?]
    [126, 49, 'png', true],
    [135, 57, 'png', true],
    [136, 53, 'png', true],
    [137, 67, 'png', true],
    [138, 74, 'png', true],
    [139, 48, 'png', true],
    [140, 0, '', false],
    [141, 0, '', false],
    [142, 0, '', false],
];

const main = async () => {
    const chapter_data = common.make_chapter_data(raw_chapter_data);
    const last_chapters = chapter_data
        .slice()
        .reverse()
        .filter((e) => e.published);

    const site = 'ouroboros';

    const webpack_config = Object.assign({}, common.webpack_config);
    webpack_config.plugins[0] = new CopyWebpackPlugin({
        patterns: [{ from: 'static-ouroboros', to: 'static' }],
    });
    webpack_config.output.path = path.resolve(__dirname, '../dist-ouroboros');
    const last_chapter = last_chapters[0];
    const second_last_chapter = last_chapters[1];

    const team = require(path.resolve(__dirname, '../src/team'));

    const tasks = [
        common.inject_entry(
            webpack_config,
            '.',
            {
                chapter_data,
                last_chapter,
                second_last_chapter,
            },
            site
        ),
    ];
    chapter_data
        .filter((e) => e.published)
        .forEach((e) => {
            tasks.push(common.make_chapter(webpack_config, e, site));
        });
    await Promise.all(tasks);
    return webpack_config;
};

module.exports = main;
