const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { last, next } = require('cheerio/lib/api/traversing');

const make_entries = (file) => {
    return [path.resolve(__dirname, '../src', file)];
};

const make_chapter = async (webpack_config, params = {}, site) => {
    webpack_config.entry['reader-scroll'] = make_entries(
        `reader-scroll/index.js`
    );
    webpack_config.entry['reader-paged'] = make_entries(
        `reader-paged/index.js`
    );
    params.site = site;
    webpack_config.plugins.push(
        new HtmlWebpackPlugin({
            filename: `chapter-${params.number}/index.html`,
            template: path.resolve(
                __dirname,
                '../src',
                'reader-scroll',
                'index.pug'
            ),
            chunks: ['common', 'reader-scroll'],
            templateParameters: params,
        })
    );
    webpack_config.plugins.push(
        new HtmlWebpackPlugin({
            filename: `chapter-${params.number}/paged/index.html`,
            template: path.resolve(
                __dirname,
                '../src',
                'reader-paged',
                'index.pug'
            ),
            chunks: ['common', 'reader-paged'],
            templateParameters: params,
        })
    );
    return webpack_config;
};

const inject_entry = async (webapck_config, page, params = {}, site) => {
    let name = 'src-' + page.split('/').join('-');
    if (page === '.') {
        name = 'src-home';
    }
    params.site = site;

    webpack_config.entry[name] = make_entries(`${page}/index.js`);
    webpack_config.plugins.push(
        new HtmlWebpackPlugin({
            filename: `${page}/index.html`,
            template: path.resolve(__dirname, '../src', `${page}/index.pug`),
            chunks: ['common', name],
            templateParameters: params,
        })
    );
    return webapck_config;
};

const webpack_config = {
    devtool: 'source-map',
    devServer: {
        quiet: false,
        disableHostCheck: true,
        noInfo: false,
        stats: {
            assets: true,
            children: false,
            chunks: true,
            chunkModules: false,
            colors: true,
            entrypoints: true,
            hash: false,
            modules: false,
            timings: false,
            version: false,
        },
    },
    entry: {
        common: [path.resolve(__dirname, '../src', 'common')],
    },
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: '[name].js',
    },
    plugins: [
        new CopyWebpackPlugin({ patterns: [{ from: 'static', to: 'static' }] }),
        new MiniCssExtractPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            url: false,
                        },
                    },
                    'postcss-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
            {
                test: /\.pug$/,
                use: [
                    {
                        loader: 'pug-loader',
                        options: {
                            root: path.join(__dirname, '../src'),
                        },
                    },
                ],
            },
            {
                test: /\.(gltf)$/,
                use: ['gltf-webpack-loader'],
            },
            {
                test: /\.glsl$/,
                use: ['webpack-glsl-loader'],
            },
            {
                test: /\.(png|jpe?g|gif|mp3|mp4|bin|glb)$/i,
                use: ['file-loader'],
            },
            {
                test: /\.(txt)$/i,
                use: ['raw-loader'],
            },
        ],
    },
};

const make_chapter_data = (raw_chapter_data) => {
    const chapter_data = raw_chapter_data.map((e, index) => {
        const next = raw_chapter_data[index + 1];
        let next_link = false;
        let external_link = false;
        if (next) {
            let next_published = next[3] || next.length === 2;
            if (next_published) {
                next_link = `/chapter-${next[0]}/`;
            }
        }

        if (e.length === 2) {
            // these are external chapters.
            e = [e[0], 0, '', true, e[1]];
        }

        return {
            number: e[0],
            limit: e[1],
            extension: e[2],
            published: e[3],
            link: e[4] || false,
            next_link,
        };
    });
    return chapter_data;
};

module.exports = {
    make_chapter,
    make_entries,
    inject_entry,
    make_chapter_data,
    webpack_config,
};
